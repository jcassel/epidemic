context("Test simulation functions")

a <- suppressWarnings(run_simulation_simple(50,3,20,10))
suppressWarnings(invisible(capture.output(b <- run_simulation_shopping_simple(50,3,20,10))))
c <- suppressWarnings(run_simulation_quarantine_simple(50,3,20,10))
suppressWarnings(invisible(capture.output(d <- run_simulation(50,3,20,10))))

format_pop <- function(pop){
  pop %>% group_by(stat) %>% count -> pop
  return(pop)
}

validate_pop <- function(pop){
  stopifnot("Pos_x must be numeric" = is.numeric(pop$pos_x))
  stopifnot("Pos_y must be numeric" = is.numeric(pop$pos_y))
  stopifnot("old_x must be numeric" = is.numeric(pop$old_x))
  stopifnot("old_y must be numeric" = is.numeric(pop$old_y))
  stopifnot("Q must be 0 or 1" = pop$Q %in% c(0,1))
  stopifnot("Shopping must be 0 or 1" = pop$shopping %in% c(0,1))
  stopifnot("Community must be numeric" = is.numeric(pop$community))
  stopifnot("d_rate must be numeric" = is.numeric(pop$d_rate))
  stopifnot("food must be numeric" = is.numeric(pop$food))
  stopifnot("Stat must be valid" = pop$stat %in% c("S","I","R","E","D"))
  stopifnot("The row number must be 50" = nrow(pop) == 50)
  return(TRUE)
}

validate_mvmt <- function(mvmt){
  stopifnot("The row number must be 550" = nrow(mvmt) == 550)
  stopifnot("x must be numeric" = is.numeric(mvmt$x))
  stopifnot("y must be numeric" = is.numeric(mvmt$y))
  stopifnot("Stat must be valid" = mvmt$stat %in% c("S","I","R","E","D"))
  return(TRUE)
}

test_that("Simulation format",{
  expect_equal(validate_simulation(a),TRUE)
  expect_equal(validate_simulation(b),TRUE)
  expect_equal(validate_simulation(c),TRUE)
  expect_equal(validate_simulation(d),TRUE)
})

test_that("Population format",{
  expect_equal(validate_pop(a$pop),TRUE)
  expect_equal(validate_pop(b$pop),TRUE)
  expect_equal(validate_pop(c$pop),TRUE)
  expect_equal(validate_pop(d$pop),TRUE)
})

test_that("Movement format",{
  expect_equal(validate_mvmt(a$mvmt),TRUE)
  expect_equal(validate_mvmt(b$mvmt),TRUE)
  expect_equal(validate_mvmt(c$mvmt),TRUE)
  expect_equal(validate_mvmt(d$mvmt),TRUE)
})

test_that("Simulation values",{
  p1 <- format_pop(a$pop)
  p2 <- format_pop(b$pop)
  p3 <- format_pop(c$pop)
  p4 <- format_pop(d$pop)
  expect_equal(p1[p1$stat == "I",]$n >= 25, TRUE)
  expect_equal(p1[p1$stat == "I",]$n <= 40, TRUE)
  expect_equal(p1[p1$stat == "R",]$n >= 5, TRUE)
  expect_equal(p1[p1$stat == "R",]$n <= 25, TRUE)
  # expect_equal(p1[p1$stat == "S",]$n >= 0, TRUE)
  # expect_equal(p1[p1$stat == "S",]$n <= 15, TRUE)
  expect_equal(p2[p2$stat == "I",]$n >= 19, TRUE)
  expect_equal(p2[p2$stat == "I",]$n <= 39, TRUE)
  expect_equal(p2[p2$stat == "R",]$n >= 10, TRUE)
  expect_equal(p2[p2$stat == "R",]$n <= 30, TRUE)
  # expect_equal(p2[p2$stat == "S",]$n >= 0, TRUE)
  # expect_equal(p2[p2$stat == "S",]$n <= 10, TRUE)
  expect_equal(p3[p3$stat == "I",]$n >= 11, TRUE)
  expect_equal(p3[p3$stat == "I",]$n <= 40, TRUE)
  expect_equal(p3[p3$stat == "R",]$n >= 5, TRUE)
  expect_equal(p3[p3$stat == "R",]$n <= 30, TRUE)
  expect_equal(p3[p3$stat == "S",]$n >= 0, TRUE)
  expect_equal(p3[p3$stat == "S",]$n <= 19, TRUE)
  expect_equal(p4[p4$stat == "I",]$n >= 0, TRUE)
  expect_equal(p4[p4$stat == "I",]$n <= 20, TRUE)
  # expect_equal(p4[p4$stat == "R",]$n >= 0, TRUE)
  # expect_equal(p4[p4$stat == "R",]$n <= 20, TRUE)
  expect_equal(p4[p4$stat == "S",]$n >= 20, TRUE)
  expect_equal(p4[p4$stat == "S",]$n <= 50, TRUE)
})
