init_com <- function(n,I0,grid_size,move_radius=1, mean_d_rate = 0.05, var_d_rate = 0, incub = FALSE, dead = FALSE,communities=4,commuter=0,I0_com=1){
  coords_x <- sample(1:grid_size,n,replace = TRUE)
  coords_y <- sample(1:grid_size,n,replace = TRUE)

  # Initiiere die Stats mit I0 mal I und n-I0 mal S; dies ist die Startkonfiguration
  # Hier werden die Stats der ersten vier künstlich verändert, da sonst der plotly plot
  # komische Sachen macht. Ist eigentlich nicht richtig, sollte man ausbessern ...
  if(incub == TRUE & dead == TRUE) {
    stat <- c(rep("I",I0),rep("S",n-I0-3),"R","E","D")
  } else if(incub == TRUE) {
    stat <- c(rep("I",I0),rep("S",n-I0-2),"R","E")
  } else if(dead == TRUE) {
    stat <- c(rep("I",I0),rep("S",n-I0-2),"R","D")
  } else {
    stat <- c(rep("I",I0),rep("S",n-I0-1),"R")
  }

  # Zahl der Individuen in den ersten I0_com Communities, diese könnten infiziert werden
  ind_in_I0_com <- floor(I0_com * (n / communities))
  # Sick-Wert für I0 Infizierte aus den ersten I0_com Communities und ind_in_I0_com - I0 Gesunde
  sick1 <- c(rep(0,I0),rep(-1,ind_in_I0_com - I0))
  times_infected1 <- c(rep(1,I0),rep(0,ind_in_I0_com - I0))
  # Nur Gesunde in nicht betroffenen Communities
  sick2 <- rep(-1,n - ind_in_I0_com)
  times_infected2 <- rep(0,n - ind_in_I0_com)
  sick <- c(sick1,sick2)
  times_infected <- c(times_infected1,times_infected2)

  # Verteile Kranke zufällig in den Communities eins bis I0
  temp <- sample(1:ind_in_I0_com,ind_in_I0_com,replace=FALSE)
  stat[1:ind_in_I0_com] <- stat[temp]
  sick[1:ind_in_I0_com] <- sick[temp]
  times_infected[1:ind_in_I0_com] <- times_infected[temp]

  commuter <- c(rep(TRUE,commuter),rep(FALSE,n - commuter))
  temp <- sample(1:n,n,replace=FALSE)
  commuter <- commuter[temp]


  # Jeder hat einen eigenen Move-Radius
  radius <- move_radius

  # Food stat, der angibt wann ein Individuum
  # Einkaufen gehen muss
  food <- sample(1:10,n,replace = TRUE)

  # Sterberate für jeden
  if(mean_d_rate - var_d_rate < 0) a <- 0 else a <- mean_d_rate - var_d_rate
  if(mean_d_rate + var_d_rate > 1) b <- 1 else b <- mean_d_rate + var_d_rate
  d_rate <- sample(seq(from = a, to = b,length.out=20),n,replace=TRUE)

  # Community-Tag
  community <- rep(1:communities,each=ceiling(n/communities),length.out=n)
  origin <- rep(1:communities,each=ceiling(n/communities),length.out=n)

  # Setze das Tibble aus den oben erstellten Spalten zusammen
  tb <- tibble(person = 1:n,
               pos_x = coords_x,
               pos_y = coords_y,
               stat = stat,
               sick = sick,
               times_infected = times_infected,
               Q = 0,
               community = community,
               origin = origin,
               travels_in = 0,
               commuter = commuter,
               ignorant = FALSE,
               radius = 1,
               d_rate = d_rate,
               food = food,
               shopping = 0,
               old_x = pos_x,
               old_y = pos_y)

  return(tb)
}

adjust_coords_com <- function(pop,grid_size,communities){
  com_x <- ceiling(sqrt(communities))
  com_y <- rep((0:(com_x-1)), each = com_x, length_out = communities)
  pop %>%
    group_by(community) %>%
    mutate(pos_x = pos_x + ((community-1) %% com_x ) * (grid_size + 20) ) %>%
    mutate(pos_y = pos_y + com_y[community] * (grid_size + 10) ) %>%
    ungroup()

}

undo_communities_com <- function(pop,grid_size,communities){
  com_x <- ceiling(sqrt(communities))
  com_y <- rep((0:(com_x-1)), each = com_x, length_out = communities)
  pop %>%
    group_by(community) %>%
    mutate(pos_x = pos_x - ((community-1) %% com_x ) * (grid_size + 20) ) %>%
    mutate(pos_y = pos_y - com_y[community] * (grid_size + 10) ) %>%
    ungroup()
}

# Erzeugt ein Tibble, welches die Bewegung der Individuen in jedem Zeitschritt speichert
# Inputs:
#   n: Größe der population
#   pop: Populationstibble, also eines, wie es von init_pop ausgegeben wird
init_mvmt <- function(n,pop){
  # Erzeuge ein Tibble
  mvmt <- tibble(person = 1:n)

  # Speichere in der Spalten x und y die Positionen der Individuen aus pop
  mvmt$x <- pop$pos_x
  mvmt$y <- pop$pos_y

  # Speichere auch deren Stats
  mvmt$stat <- pop$stat
  # mvmt$commuter <- pop$commuter
  # mvmt$shopping <- pop$shopping

  # mvmt speichert die Zeitentwicklung der der Individuen. Deshalb wird bei der
  # Erstellung das Zeitattribut auf 1 gesetzt
  mvmt$t <- 0
  return(mvmt)
}

init_counter <- function(tb) {
  counter <- tibble(time = 0,
                    S = nrow(filter(tb,stat == "S")),
                    E = nrow(filter(tb,stat == "E")),
                    I = nrow(filter(tb,stat == "I")),
                    R = nrow(filter(tb,stat == "R")),
                    D = nrow(filter(tb,stat == "D")),
                    Q = nrow(filter(tb,Q == 1)),
                    cases = sum(unname(as.vector(tb$times_infected))))
  return(counter)
}

# Plot für ein tibble tb, wie es von init_pop ausgegeben wird
plot_pop <- function(tb){
  stopifnot("tb muss ein Tibble sein." = is_tibble(tb))
  ggplot(tb,aes(x=pos_x,y=pos_y)) + geom_point(aes(color=stat))
}

# Erzeugt ein Gitter basierend auf einem Tibbble.
# tb ist ein Tibble, wie es von init_pop
# ausgegeben wird und grid_size gibt die Größe des Gitters an
# Das Gitter ist eine Matrix der Größe grid_size*grid_size. Auf den
# Positionen auf denen keine Personen sind, steht eine 0. Auf
# Positionen mit einer Person steht der Index der Person, die dort ist.
generate_grid <- function(tb,grid_size){
  grid <- matrix(0,ncol=grid_size,nrow=grid_size)
  tb2 <- filter(tb, Q != 1, stat != "D")
  v <- tb2$person
  if(length(v) == 0) return(grid)
  for(k in v) {
    grid[tb$pos_y[k],tb$pos_x[k]] <- k
  }
  return(grid)
}


# Platziert Individuen
# Eine Position kann nicht mehrfach besetzt sein
#
# Wird von run_simulation_everything benötigt
#
# Eingabe:
# - tb: von init_com zurückgegebenes Tibble (benötigt werden die Spalten person pos_x,pos_y,old_x,old_y)
# - grid_size: Seitenlänge des Gitters
# - communities: Anzahl der Communities
# - shop: von shops_coords zurückgebene Koordinaten des Shops
#
# Ausgabe
# - Tibble mit platzierten Individuen

place <- function(tb,
                  grid_size,
                  communities = 1,
                  shop = 0) {
  # Es gibt nrow(tb) Individuen, zu jedem Individuum gehört eine Zahl zwischen 1 und nrow(tb)
  # Diese ist in der Spalte person vermerkt
  # Besetze daher den Shop mit nrow(tb) + 1 (keine zu einem Individuum gehörige Nummer),
  # damit kein Individuum im Shop platziert wird
  x <- nrow(tb) + 1
  # Setze die aktuelle Position auf 0, da derzeit noch Mehrfachbelegung möglich
  tb$pos_x <- 0
  tb$pos_y <- 0
  # Platziere für jede Community gesondert
  # Die Koordinaten werden später durch adjust_coords_com angepasst
  for (l in 1:communities) {
    tb2 <- filter(tb, community == l)
    # Falls es dort keine Individuen gibt, weil es mehr Communities als Individuen gibt,
    # Springe weiter
    if (nrow(tb2) == 0)
      next
    v <- tb2$person
    # Initialisiere Matrix der Positionen
    mat <- matrix(0, nrow = grid_size, ncol = grid_size)
    # Trage die Position des Shops in die Matrix ein
    if(shop[1] != 0) { mat[shop, shop] <- x }
    for (k in v) {
      bool <- TRUE
      while (bool) {
        # Ziehe Position für Individiuum
        temp <- sample(1:grid_size, 2, replace = TRUE)
        # Stelle vor der Zuweisung der Position sicher,
        # dass sich dort niemand befindet
        # Übertrage die Position in das Tibble
        # Aktualisiere die Positionsmatrix
        if (mat[temp[1], temp[2]] == 0) {
          tb$pos_y[k] <- temp[1]
          tb$pos_x[k] <- temp[2]
          tb$old_y[k] <- temp[1]
          tb$old_x[k] <- temp[2]
          mat[temp[1], temp[2]] <- tb$person[k]
          bool <- FALSE
        }
      }
    }
  }
  return(tb)
}

# Berechnet die Koordinaten des Shops
# Der Shop wird zentriert
#
# Wird von run_simulation_everything benötigt, um
# - Individuen auf dem Gitter zu platzieren (place)
# - shoppers zum Shop zu bringen (get_groceries)
# - zu verhindern, dass non-shoppers in den shop stolpern (move_multi,return_from_quarantine,arrive,commute)
# - den shop als Rechteck in den Plot einzufügen (get_rectangles)
#
# Eingabe:
# - grid_size: Seitenlänge des Gitters
# - central_size: Seitenlänge des Shops
#
# Ausgabe: Koordinaten des Shops (Vektor)

shop_coords <- function(grid_size, central_size) {
  # shop auf 0, falls kein Shop gewünscht
  if(central_size == 0) {
    shop <- 0
  } else if(central_size == 1) { # shop genau 1, platziere Shop auf dem Punkt, der am ehesten ein Mittelpunkt ist
    # bestimme Mittelpunkt des Gitters bzw. etwas, das nah dran ist, Mittelpuntk zu sein
    shop <- ceiling(grid_size / 2)
  } else if (central_size %% 2 == 0) { # central_size gerade, teile durch 2 und gehe vom Mittelpunkt aus so viele Schritte nach rechts und links
    middle <- ceiling(grid_size / 2)
    left <- central_size / 2
    right <- central_size / 2 - 1
    start <- middle - left
    end <- middle + right
    shop <- start:end
  } else { # central_size ungerade, gleich aus, dass man nicht gleich viele Schritte vom Mittelpunkt aus nach rechts und links gehen kann
    middle <- ceiling(grid_size / 2)
    left <- (central_size - 1) / 2
    right <- (central_size + 1) / 2 - 1
    start <- middle - left
    end <- middle + right
    shop <- start:end
  }
  return(shop)
}


