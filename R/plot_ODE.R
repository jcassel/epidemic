#'@export
#'
#'@title plot_model
#'
#'@description This function plots the specific graphs of the tibbles created by our simulation functions, like SI_simulation.
#'
#'@param tb A tibble with numeric entries and created by one of our simulation functions.
#'@param tag A character- vector, which can only be "SI", "SIS", "SIR" or "SEIR".
#'
#'@examples
#'plot_model(SI_simulation(0,100,0.1,0.4,1000), "SI")
#'plot_model(SIS_simulation(0,120,0.01,0.06,15,0.6,2000000),"SIS")
#'plot_model(SIR_simulation("Euler method (first-order)",0,100,1000,0.4,0.04,1,0.01),"SIR")
#'plot_model(SEIR_simulation("Runge-Kutta method (fourth-order)",110194,0.196266,0.07137758743,0.14184397163,0,41,0,280),"SEIR")

#'@return A graph of the chosen epidemic model.

plot_model <- function(tb,tag){
  if(tag == "SEIR"){
    tb <- pivot_longer(tb,c(S,E,I,R),names_to = "parameter",values_to = "individuals")
    return(ggplot(data = tb,mapping = aes(x=time,y=individuals,color = parameter)) + geom_line(size=1.2))
  }
  if(tag == "SIR"){
    tb <- pivot_longer(tb,c(S,I,R),names_to = "parameter",values_to = "individuals")
    return(ggplot(data = tb,mapping = aes(x=time,y=individuals,color = parameter)) + geom_line(size=1.2))
  }
  if(tag == "SIS"){
    tb <- pivot_longer(tb,c(S,I),names_to = "parameter",values_to = "individuals")
    return(ggplot(data = tb,mapping = aes(x=time,y=individuals,color = parameter)) + geom_line(size=1.2))
  }
  if(tag == "SI"){
    tb <- pivot_longer(tb,c(S,I),names_to = "parameter",values_to = "individuals")
    return(ggplot(data = tb,mapping = aes(x=time,y=individuals,color = parameter)) + geom_line(size=1.2))
  }
}
